const min = (arr) => Math.min(...arr);

module.exports = min;