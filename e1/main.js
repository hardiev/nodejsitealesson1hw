const min = require("./min");
const summ = require("./summ");
const abs = require("./abs");

let array = [-8, 4, 625, 75, 4];

console.log(min(array) + "\n" + summ(array) + "\n" + abs(array[0]));