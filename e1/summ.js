const res = (arr) => arr.reduce((sum, cur) => sum + cur);

module.exports = res;